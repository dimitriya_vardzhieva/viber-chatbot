# Viber Chatbot

## 1. Description

Viber chatbot named 'Zhiri' that allows you to receive certain information about me - the developer of the bot. 

It can be accessed via the deep link mentioned below.

Since the bot is still a baby it understands only a few words and responds according to it's current dictionary capacity.

For interactions try tapping the buttons or use some kew words like: hobby or places.

This is a personal project so that I can have some fun and experiment with code after graduating from Telerik Academy.

### 2. Links

 - [deep link](viber://pa?chatURI=zhiri)
 - [server](https://viber-chat-bot-demo.herokuapp.com/)

** I'm using a free service to host the app, so some malfunctions or downtime might be experienced.

### 3. Creator

- Dimitriya Vardzhieva

### 4. Core Back-end Technologies
 - Node.js
 - Express
 - MariaDB
