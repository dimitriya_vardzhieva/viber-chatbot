import dotenv from 'dotenv';
import mariadb from 'mariadb';

const config = dotenv.config().parsed;

// const db = mariadb.createPool(
//     {
//         port: config.DBPORT,
//         database: config.DATABASE,
//         user: config.USER,
//         password: config.PASSWORD,
//         host: config.HOST,
//         connectionLimit: 2,
//     },
// );

// export default db;

export const PORT = config.PORT;

