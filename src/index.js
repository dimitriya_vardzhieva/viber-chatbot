import viberBot from 'viber-bot';
import express from 'express';
import { PORT } from '../config.js';
import { getCurrentBitcoinValue, getCurrentEthereumValue } from './requests.js';

const keyboard = {
    "Type": "keyboard",
    "DefaultHeight": true,
    "Buttons": [
        {
            "ActionType": "reply",
            "ActionBody": "BTC",
            "Text": "BTC",
            "TextSize": "regular"
        },
        {
            "ActionType": "reply",
            "ActionBody": "ETH",
            "Text": "ETH",
            "TextSize": "regular"
        }
    ]
};

const customRichMedia = (price, change) => {
    const priceColor = change < 0 ? '#ff0000' : '#83ff00';

    const SAMPLE_RICH_MEDIA = {
        "ButtonsGroupColumns": 6,
        "ButtonsGroupRows": 2,
        "BgColor": "#FFFFFF",
        "Buttons": [{
            "ActionBody": "",
            "ActionType": "none",
            "BgMediaType": "picture",
            "Text": `${price}`,
            "BgColor": "#F5F5DC",
            "Rows": 1,
            "Columns": 6
        }, {
            "ActionBody": "",
            "ActionType": "none",
            "BgColor": priceColor,
            "Text": `${change}`,
            "TextOpacity": 60,
            "Rows": 1,
            "Columns": 6
        }]
    };

    return SAMPLE_RICH_MEDIA
};

const app = express();
const ViberBot = viberBot.Bot;
const BotEvents = viberBot.Events;
const TextMessage = viberBot.Message.Text;
const RichMediaMessage = viberBot.Message.RichMedia;

// Create bot
const bot = new ViberBot({
    authToken: process.env.API_KEY,
    name: 'Zhiri',
    avatar: 'https://bg.wikipedia.org/wiki/Android#/media/%D0%A4%D0%B0%D0%B9%D0%BB:Android_robot_head.svg'
});

app.use("/chatbot", bot.middleware());
app.get('/test', (req, res) => {
    res.send('OK')
})
app.listen(process.env.PORT || PORT, () => {
    console.log(`App is listening on port ${PORT}`);
    bot.setWebhook('https://viber-chat-bot-demo.herokuapp.com/chatbot')
        .then(() => console.log('web hook'))
        .catch(e => console.log(e))
});

bot.onSubscribe(response => {
    say(response, `Hi there ${response.userProfile.name}. I am ${bot.name}!`);
});

bot.on(BotEvents.SUBSCRIBED, response => {
    response.send(new TextMessage(`Thanks for subscribing, ${response.userProfile.name}`, keyboard));
});

bot.onConversationStarted((userProfile, isSubscribed, context, onFinish) => {
    console.log(userProfile)
    bot.sendMessage(userProfile,
        new TextMessage(`Hi, ${userProfile.name}! Nice to meet you.`, keyboard)
    ).then(result => {
        console.log(result)
    })
        .catch(err => { console.log(err) })
    console.log('Conversation started.')
}
);

bot.on(BotEvents.MESSAGE_RECEIVED, async (message, response) => {
    if (!(message instanceof TextMessage)) {
        response.send(new TextMessage(`Sorry. I can only understand text messages.`, keyboard));
        return;
    };
    if (message.text !== 'BTC' && message.text !== 'ETH') {
        response.send(new TextMessage(`I know only two words 'BTC' and 'ETH'. Wanna talk to me write one of those.`, keyboard));
        return;
    };
    if (message.text === 'BTC') {
        const bitcoinValue = await getCurrentBitcoinValue();
        response.send(new RichMediaMessage(customRichMedia(bitcoinValue.o, bitcoinValue.o - bitcoinValue.h[1]), keyboard));
        return;
    };
    if (message.text === 'ETH') {
        const ethereumValue = await getCurrentEthereumValue();
        response.send(new RichMediaMessage(customRichMedia(ethereumValue.o, ethereumValue.o - ethereumValue.h[1]), keyboard));
        return;
    };
    
});


