export const SAMPLE_RICH_MEDIA = {
	"ButtonsGroupColumns": 6,
	"ButtonsGroupRows": 2,
	"BgColor": "#FFFFFF",
	"Buttons": [{
		"ActionBody": "http://www.website.com/go_here",
		"ActionType": "open-url",
		"BgMediaType": "picture",
		"Image": "http://www.images.com/img.jpg",
		"BgColor": "#000000",
		"TextOpacity": 60,
		"Rows": 4,
		"Columns": 6
	}, {
		"ActionBody": "http://www.website.com/go_here",
		"ActionType": "open-url",
		"BgColor": "#85bb65",
		"Text": "Buy",
		"TextOpacity": 60,
		"Rows": 1,
		"Columns": 6
	}]
};

const message = new RichMediaMessage(SAMPLE_RICH_MEDIA);

// {
//     "receiver": "nsId6t9MWy3mq09RAeXiug==",
//     "type": "rich_media",
//     "min_api_version": 7,
//     "rich_media": {
//         "Type": "rich_media",
//         "ButtonsGroupColumns": 1,
//         "ButtonsGroupRows": 1,
//         "BgColor": "#FFFFFF",
//         "Buttons": [
//             {
//                 "Columns": 6,
//                 "Rows": 2,
//                 "Text": "<font color=#FF0000><b>Here will be the value ot BTC or ETH in the appropriate color</font>"             
//             }
//         ]
//     }
// }