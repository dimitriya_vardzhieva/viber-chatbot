import fetch from 'node-fetch';
import { BITCOIN, ETHEREUM } from './constants.js'

export const getCurrentBitcoinValue = async () => {
  try {
    const res = await fetch(`${BITCOIN}`);
    const { error, result } = await res.json();
    error.length > 0 && console.log('Error from Bitcoin GET ' + e);

    return result.XXBTZUSD;
  } catch (error) {
    console.log(error);
  }
};

export const getCurrentEthereumValue = async () => {
  try {
    const res = await fetch(`${ETHEREUM}`);
    const { error, result } = await res.json();
    error.length > 0 && console.log('Error from Ethereum GET ' + e);

    return result.XETHZUSD;
  } catch (error) {
    console.log(error);
  }
}



